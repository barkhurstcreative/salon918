page_heading: 'Services & Prices'
page_subheading: 'Salon 918 is a full-service salon focused on building genuine relationships with our clients.'
page_description: 'Our <a href="/stylists"><u>stylists</u></a> ESPECIALLY love color and provide lowlighting, foilyage, highlights, babylights, brondage, balayage, sunlights, ombre, sombre, double-process color, fantasy colors, mermaid hair, oil slicking and more.'
specials_heading: 'UNITE Products Special'
specials_description: 'Every day of the year: "Buy 2 Get 1 Free"'
product_details: |

  <p>Check out our <u><a href="https://www.instagram.com/stories/highlights/17868746605586900/" target="_blank">UNITE Product Highlights</a></u> on Instagram or ask your stylist for help choosing the perfect collection!
  </p>
feature_product:
  - /assets/img/unite-product-specials-in-morgantown.jpg
faqs_heading: 'Frequently Asked Questions'
faqs_description: 'If you''re ready to make an appointment or have questions about our services or policies, please give us a call at <a href="tel:{{ phone }}">{{ phone }}</a>.'
meta_title: 'Services and Pricing | Hair and Beauty Salon in Morgantown, WV | Salon 918'
meta_description: 'Absence makes the hair grow longer. Call to book your appointment (304) 599-3773.'
faq_description: 'If you''re ready to make an appointment or have questions about our services or policies, please give us a call at <a href="tel:{{ phone }}">{{ phone }}</a>.'
template: services
title: Services
fieldset: page_services
id: 627585d0-0cfd-442f-93e7-5977cb88b3c3
