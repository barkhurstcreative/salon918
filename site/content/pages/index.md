headline:
  - /assets/img/let-your-hair.svg
subheadline: 'Call to make your appointment <a href="tel:{{ phone }}">{{ phone }}'
image1:
  - /assets/img/homepage-hair.png
image2:
  - /assets/img/homepagehair-2.png
image3:
  - /assets/img/homepagehair-3.png
about_heading: 'Full-Service Salon'
about_subheading: 'Morgantown, West Virginia'
about_description: |
  <p class="mb-5"><a href="/stylists">Salon 918 stylists</a> specialize in Keratin Hair Straightening, Highlights, Color, Perms, Brazilian Blow-Outs, Balayage, Aqua Extensions Tape-Ins, Formal Hair Styles, Barbering, Waxing, Manicures, Weddings and Microblading.
  </p>
  <p>Our salon loves our <a href="/services#products">UNITE hair products</a>, Redken and Wella hair color, and GHD and Dyson Hair products.
  </p>
button_text: 'Explore Services & Prices'
button_url: /services
love_heading: 'Leave Loving Your Hair'
love_image:
- /assets/img/red-hair-color-salon-in-morgantown.jpg
love_text: |
  <p>
  	At Salon 918 our goal and core belief is that you can love your hair! Our <a href="/stylists.html"><u>stylists</u></a> know the importance of your "hair consultation" and will genuinely listen to you and take the time to understand what you’re seeking.
  </p>
  <p>
  	You can be assured that we’ll always be 100% honest about what is possible with your hair.
  </p>
  <p>Explore our <a href="/services.html"><u>hair services</u></a> and prices.
  </p>
testimonials_heading: 'From Our Clients'
testimonials_description: |
  <p class="mb-0">Thanks for sharing the love; you're the reason we do what we do!
  </p>
  <p><a href="/stylists"><u>Meet the Salon 918 Stylists</u></a>
  </p>
social_heading: 'Our Latest on Social Media'
social_wall_script: |
  <script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
  <link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
  <ul class="juicer-feed" data-feed-id="hairabovetheneck"><h1 class="referral"><a href="https://www.juicer.io">Powered by Juicer</a></h1></ul>
meta_title: 'Hair Cuts, Hair Color, Hair Styling | Morgantown Salon | Salon 918'
meta_description: 'Let your hair do the talking. Call us to book your next hair cut, hair color or hairstyle appointment -- for women, men and children. Call 304.599.3773'
gallery:
- /assets/img/homepage-hair.png
- /assets/img/homepagehair-2.png
- /assets/img/homepagehair-3.png
display_announcement: false
form_heading: 'Our Latest on Social Media'
template: home
title: Home
fieldset: page_home
id: db0ae4e3-4f10-4802-bc40-0b880cbf02c7
