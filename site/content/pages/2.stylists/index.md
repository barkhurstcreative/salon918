owners_heading: Owners
owners_description: |
  <p>Angie Barnhart and Reilly Scotchel are the owners of Salon 918.
  </p>
team_heading: 'Our Team'
team_description: |
  <p>Meet the talented stylists who call Salon 918 home. Our collective <a href="/services"><u>specialties </u></a> include: Keratin Hair Straightening, Highlights, Color, Perms, Brazilian Blow-Outs, Balayage, Aqua Extensions Tape-Ins, Formal Hair Styles, Barbering, Waxing, Manicures, and Microblading.
  </p>
testimonials_heading: 'From Our Clients'
testimonials_description: |
  <p>Read what our BEAUTIFUL clients have to say about our talented stylists.
  </p>
meta_title: 'Talented Hair Stylists at Salon 918 | Morgantown Salon | West Virginia'
meta_description: 'Come meet our sophisticated and talented stylists that genuinely listen and take care of your hair concerns. Schedule your appointment today.'
testimonial_description: |
  <p>Read what our BEAUTIFUL clients have to say about our talented stylists.
  </p>
template: stylists
heading: Owners
title: Stylists
fieldset: page_stylists
id: c0f7edcd-adc1-4de9-90f6-875579dec71c
