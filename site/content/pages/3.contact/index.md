---
image_of_salon:
  - /assets/img/toni-exquisite-hair-now-918-salon-in-morgantown-wv.jpg
announcement_heading: "Covid-19 Updates"
display_announcement: true
announcement_text: |
  <p>Please call us to schedule your appointment as we are not able to accept walk-ins. Masks are required. We're looking forward to seeing you!
  </p>
  <p><a href="/announcement"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Read more about our new protocols</a>.
  </p>
announcement: |
  <p>Please call us to schedule your appointment as we are not able to accept walk-ins. Masks are required. We're looking forward to seeing you!</p>
  <p><a href="/covid19"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Read more about our new protocols</a>. </p>
directions_heading: Directions
directions_description: |
  <p>Salon 918 is conveniently located in the Suncrest Professional Building off Route 705 near Ruby Memorial Hospital, Mon General Hospital, and Mountaineer Field at 918 Chestnut Ridge Road Suite 2, Morgantown, WV. (Formally Toni Exquisite Hair)
  </p>
google_map_code: '<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ95va6w97NYgR4LbzQKseBXg&key=AIzaSyB5yAyByzhdDMW5eIXxunenByoMOdpHys4" allowfullscreen></iframe>'
form_heading: "Get in Touch"
form_description: |
  <p>Ready to make an appointment or have a question? Feel free to give us a call or send us a message.
  </p>
meta_title: "Book Hair Appointment | Morgantown Salon | Salon 918"
meta_description: "Conveniently located near both hospitals, Salon 918 specializes in hair color and cuts for women, men, and children. Call 304.599.3773"
template: contact
title: Contact
fieldset: page_contact
id: de627bca-7595-429e-9b41-ad58703916d7
---

Ready to make an appointment or have a question? Feel free to give us a call or send us a message.
