content_main:
  -
    type: text
    text: '<p>To Our Loyal Customers,</p><p>Due to the current pandemic, we are respectfully implementing the following measures to help reduce the risk and spread of infection:</p><ol><li>Services will not be rendered to anyone who is ill or exhibiting signs of illness.</li><li>All of our clients will be asked, prior to service, if they been ill or exposed to anyone who has been ill. If so, please wait 14 days before you schedule an appointment.</li><li>Although it is not possible for you to wear a mask during the entire time of your visit, we suggest it when feasible.</li></ol><p>Please call us to schedule your appointment, as we are not able to accept walk-ins. Our phone number is <a href="tel:{{ phone }}">{{ phone }}</a>.</p><p>Our Salon will conscientiously promote a sanitized and safe environment. We will limit the number of people in the salon at the same time. Our stylists will be required to wear Personal Protective Equipment which includes masks.</p>'
meta_title: 'Covid-19 Protocols at Salon 918'
meta_description: 'We''re open and taking appointments. We have a few new protocols; you can read about them here. Questions? Call us at 304.599.3773.'
template: default
title: Covid-19
fieldset: default
id: 7d763b4c-7631-4d27-b03a-379377c9d12c