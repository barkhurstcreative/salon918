serviceprice:
  -
    service: 'Short Hair Perm'
    price: 'starting at $95'
  -
    service: 'Spiral Perm'
    price: $150
images:
  - /assets/img/formal-updos-in-morgantown-salon.jpg
title: Perms
id: 982dff32-a2a4-416d-bb9a-6a4310d4dfaa
tags:
  - perms
