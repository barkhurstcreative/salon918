serviceprice:
  -
    service: Blow-dry
    price: 'starting at $25'
    details: 'Includes shampoo'
  -
    service: 'Blow-dry + Extra Styling'
    price: 'Add $15'
  -
    service: 'Blow-dry + Formal Styling'
    price: 'Add $40'
images:
  - /assets/img/blowout-in-morgantown-wv.jpg
title: Blow-dry
id: abff6c6b-cc28-4059-8e46-23b5e9c961fe
tags:
  - blowdry
