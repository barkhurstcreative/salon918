serviceprice:
  -
    service: Eyebrow/Lip/Chin
    price: $30
  -
    service: Lip
    price: $15
  -
    service: Brow
    price: $20
  -
    service: Eyebrow/Lip
    price: $25
  -
    service: Microblading
    price: $350
    details: 'Meticulously have your brows designed. Free consultation; takes about 3 hours. Price includes 30-day touch-up.'
images:
  - /assets/img/microblading-brows.jpg
  - /assets/img/microblading-brows3.jpg
  - /assets/img/microblading-brows2.jpg
  - /assets/img/microblading-brows4.jpg
title: 'Waxing and Microblading'
id: 70930587-a55f-4e71-a1da-23794875bbf2
tags:
  - waxing
