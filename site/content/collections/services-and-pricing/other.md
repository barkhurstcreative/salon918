serviceprice:
  -
    service: 'Extensions hourly'
    price: $100/hour
    details: 'Put-in or take-out Aqua Tape-Ins'
  -
    service: 'Extensions hair'
    price: 'starting at $115'
    details: 'Per pack of hair, dependent on length'
  -
    service: Brazilian
    price: $250
  -
    service: Keratin
    price: $250
  -
    service: 'Conditioning Treatment w/ Style'
    price: $45
images:
  - /assets/img/aqua-extensions-in-morgantown.jpg
title: 'Other Hair Services'
id: a56e3a05-da2d-46a7-9d75-c1863919f9d6
tags:
  - other
