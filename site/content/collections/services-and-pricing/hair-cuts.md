serviceprice:
  -
    service: 'Female Cut'
    price: 'Starting at $55'
  -
    service: 'Male Cut'
    price: 'Starting at $30'
  -
    service: 'Child Cut'
    price: $15
    details: '3 and below'
  -
    service: 'Child Cut'
    price: $20
    details: '12 and under'
images:
  - /assets/img/womens-hair-cut-in-morgantown-wv.jpg
  - /assets/img/long-hair-cut-and-color-morgantown-wv.jpg
  - /assets/img/best-female-hair-cut-in-morgantown-wv.jpg
  - /assets/img/mens-hair-cut-in-morgantown.jpg
title: 'Hair Cuts'
id: 96187ec4-3a3d-4aea-81cf-0507b99ee558
tags:
  - hair-cuts
