serviceprice:
  -
    service: 'IN-SALON HAIR'
    price: 'starting at $65'
  -
    service: 'IN-SALON MAKEUP'
    price: 'starting at $65'
  -
    service: 'IN-TOWN HAIR'
    price: 'starting at $85'
  -
    service: 'IN-TOWN MAKEUP'
    price: 'starting at $85'
  -
    service: 'OUT-OF-TOWN HAIR'
    price: 'starting at $100'
  -
    service: 'OUT-OF-TOWN MAKEUP'
    price: 'starting at $100'
images:
  - /assets/img/wedding-hair-stylists-in-morgantown.png
  - /assets/img/formal-gala-hair-for-weddings-and-events.jpg
  - /assets/img/formal-updos-in-morgantown-salon.jpg
title: 'Weddings and Formal'
id: 62f88dab-e77a-4722-9f58-106dc7f85d87
tags:
  - formal
