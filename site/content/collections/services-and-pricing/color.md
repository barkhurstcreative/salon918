serviceprice:
  -
    service: 'Base Color'
    price: 'starting at $90'
  -
    service: 'Highlights Partial'
    price: 'starting at $120'
  -
    service: 'Highlights Full'
    price: 'starting at $150'
  -
    service: 'Balayage Partial'
    price: 'starting at $115'
  -
    service: 'Balayage Full'
    price: 'starting at $175'
  -
    service: 'Balayage with Highlights'
    price: 'starting at $150'
  -
    service: 'Color Gloss'
    price: $30
  -
    service: 'Kids'' Fun Color'
    price: '$10 a foil'
    details: 'Fun Streaks/Wild Color'
images:
  - /assets/img/blonde-color-female-hair-styles.jpg
  - /assets/img/best-straight-hair-color-and-cuts-morgantown-wv.jpg
  - /assets/img/blonde-color-highlights-morgantown-salon.jpg
  - /assets/img/best-brunette-hair-cuts-and-color-in-morgantown.jpg
  - /assets/img/blonde-hair-color-in-morgantown.jpg
  - /assets/img/blonde-highlights-morgantown-salon.jpg
title: Color
id: 4f78ffe2-c89b-4314-988b-06585c8b0d6a
tags:
  - color
