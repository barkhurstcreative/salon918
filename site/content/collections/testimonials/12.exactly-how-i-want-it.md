title: 'Exactly how I want it'
quote: 'Whenever I get my hair colored it looks exactly how I want it. They are all so friendly and really make you feel at home like you''ve made an instant friend.'
credit: 'E. Cochran, Google Review'
link: 'https://goo.gl/maps/kmuJxa8oHVWCSTz86'
tags:
  - general
id: 10c3cd6c-108e-4bd9-a7ca-b54d7e3532f3
