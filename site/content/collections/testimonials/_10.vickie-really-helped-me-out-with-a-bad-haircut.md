title: 'Vickie really helped me out with a bad haircut.'
quote: 'Vickie really helped me out with a bad haircut. She took her time and fixed it. She was very professional and assured me it would be okay.  Thanks and I will be back!'
credit: 'Daisy Duke, Google Review'
link: 'https://www.google.com/maps/contrib/107219336434473624883/place/ChIJ95va6w97NYgR4LbzQKseBXg/@39.5804708,-80.0798029,12z/data=!4m6!1m5!8m4!1e1!2s107219336434473624883!3m1!1e1?hl=en-US'
tags:
  - stylist
id: 3851697d-e5c3-4299-b9c3-bd2116d1dde9
