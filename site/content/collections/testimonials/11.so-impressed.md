title: 'So impressed'
quote: 'I went in for my first cut since the salon reopened, and I was so impressed by the cleanliness and all of the staff taking the necessary precautions in this crazy time.'
credit: 'S. Chapell, Facebook'
link: 'https://www.facebook.com/hairabovetheneck/posts/10158301519329183'
tags:
  - general
id: 7173cdae-107c-4952-82fe-a5b5d291d953
