title: 'Shelby  - Sweet'
quote: 'Shelby is super sweet and extremely patient! I came in not knowing what I really wanted -- I know, totally against etiquette when visiting the salon --  and she did a nice job giving me ideas and explaining the process. I LOVE my cut and color!'
credit: 'Hannah K, Yelp'
link: 'https://www.yelp.com/biz/tonis-exquisite-hair-morgantown'
tags:
  - stylist
id: 9447ff02-26e1-4b47-84b7-af3770285685
