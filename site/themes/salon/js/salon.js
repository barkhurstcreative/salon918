// ;(function( $ ) {

//This simple script handles the header nav menu modal
//also covered nav sub menu selection
//Dependencies: jQuery
$(document).ready(function () {

    
  $('.burger').click(function () {
    $('header').toggleClass('clicked');
    $('body').toggleClass('modal-open');
  });

  window.onhashchange = anchor_change;

  function anchor_change() {
    $('nav ul li').removeClass('selected');
    $('nav ul li').addClass('notselected');
    $('body').removeClass('modal-open');
    $('header').removeClass('clicked');
    $(this).toggleClass('selected');
    $(this).removeClass('notselected');
  }

  $('nav ul li a').click(function () {
    $('nav ul li').removeClass('selected');
    $('nav ul li').addClass('notselected');
    $('body').removeClass('modal-open');
    $('header').removeClass('clicked');
    $(this).toggleClass('selected');
    $(this).removeClass('notselected');
  });



  // FAQS Accordion -- https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_accordion_symbol
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }
});




// SCROLL TO THE TOP
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

// Back to Top Button -  When the user scrolls down 20px from the top of the document, show the button
// window.onscroll = function() {
//   scrollFunction() {};
// };

// function scrollFunction() {
//   if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
    
//     document.getElementById("totop").style.display = "block";
//   } else {
//     document.getElementById("totop").style.display = "none";
//     behavior: 'smooth';
//   }
// }


var singleton = {};
var timeout = singleton;

window.onscroll = windowScroll;

function windowScroll () {
  var toTop = document.getElementById('totop');
  toTop.style.display = ((window.scrollY > 0) ? "block" : "none");
}

function scrollStep () {
  var y1 = window.scrollY - 1000;
  window.scrollTo(0, y1);

  if (y1 > 0) {
    timeout = window.setTimeout(scrollStep, 100);  
  } else if (timeout != singleton) {
    window.clearTimeout(timeout);   
  }
}
var docWidth = document.documentElement.offsetWidth;

